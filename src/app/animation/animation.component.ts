import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.css'],
  animations:[
      trigger('pop', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('3000ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('3000ms', style({ opacity: 0 }))
      ])
    ])
  ] 
})
export class AnimationComponent implements OnInit {
  @Input('data') commenti:string[];
  brani: string[];
  current: string = "state1";

  constructor() {
  }

  action() {
    this.brani = this.commenti;
  }

  disappear() {
    this.brani = [];
  }


  ngOnInit() {
  }

}
