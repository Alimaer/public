import { Component, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './_models/post';
import { from, of } from 'rxjs'
import { switchMap, filter, concatMap } from 'rxjs/operators';
import { EventEmitter } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  baseURL: string;
  data1: Post[];
  body1: string;
  comments: string[];
  i: number;

  constructor(private http: HttpClient) {
    this.baseURL = "https://jsonplaceholder.typicode.com";
    this.body1 = "";
    this.comments = [];
    this.i = 0; 
    this.http.get(this.baseURL + "/posts").subscribe(response => {
      this.data1 = (<Post[]>response);
    this.body1 = this.TagliaCuci(this.data1, this.body1);
      }
    );

    this.http.get(this.baseURL + "/posts").pipe(
        switchMap((post:any) => from(post)),
        filter((singlePost:any) => (singlePost.id % 2 == 0))
      ).subscribe((post: any) => {}
    );

    this.http.get(this.baseURL + "/posts").pipe(

      /* nel prossimo comando abbiamo:
       * un observable formato da molti post che viene ricevuto da switchMap;
       * nella funzione di switchMap, from() spezzetta gli elementi del contenuto dell'observable iniziale
       *  a causa del from(), gli elementi del contenuto (observable) escono dallo switchmap uno alla volta 
       * il filtro riceve un elemento e valuta se farlo passare
       * il concatmap riceve i post pari e fa la chiamata ai comments. Al contrario di switchMap, che ad ogni uscita
       * cancella quella precedente, concatMap le unisce e alla fine restituisce un unico observable
       * alla fine mettiamo un altro switchMap che selezioni l'ultimo dei commenti
       * nella subscription inseriamo il comando di stampa in console
       */ 

        switchMap((post:any) => from(post)), // swtichMap è un operatore che riceve un observable e restituisce
                                             // un altro observable, come risultato di una funzione interna
        filter((singlePost:any) => (singlePost.id % 2 == 0)),
        concatMap((evenPost:any) => this.http.get(this.baseURL + "/comments?postId=" + evenPost.id)),
                                             // qui passiamo al concatMap perchè usciamo dal nostro codice, facciamo
                                             // una chiamata rest, asincrona. Se avessimo utilizzato switchMap le chiamate
                                             // sarebbero state cancellate prima di aver potuto andare a buon fine 
        switchMap((comments:any) => of(comments[comments.length-1]))
      ).subscribe((lastComment) => {
        this.comments.push(<string>lastComment.body);
      }
    );
  }

  TagliaCuci(data: Post[], body: string) {
  for(this.i = 0; this.i <= data.length-1; this.i++) {
        body = body + data[this.i].body;
      }
      return(body);
    } 
}
  

